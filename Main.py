import tkinter

# ===== VARIABLES =====

counter = 0
cpc = 1

# ===== UPGRADE 1 VARIABLES =====

up1price = 100
up1cpc = 1
up1maxbuys = 5

# ===== FUNCTIONS =====


def counterup(event):
    global counter
    global cpc
    if event=="space":
        counter = counter + cpc
    else:
        counter = counter + cpc
    print(counter)


# ===== UPGRADE 1 FUNCTION =====

def cpc1up(event):
    global counter
    global cpc
    global up1cpc
    global up1price
    global up1maxbuys
    if (counter >= up1price) and (up1maxbuys > 0):
        counter = counter - up1price
        cpc = cpc + up1cpc
        up1price = up1price + 50
        up1maxbuys = up1maxbuys - 1
        up1.update()
    else:
        print("need to add display on screen")

root = tkinter.Tk()
root.title("Stefix Clicker")
root.geometry("800x600")
b1 = tkinter.Button(root, text="test", width=20, height=10)
b1.grid(row=0,column=0)
b1.bind("<Button-1>", counterup)
up1 = tkinter.Button(root, text="Upgrade1 - cost "+str(up1price)+", cpc+1, max buys 5")
up1.grid(row=0,column=1)
up1.bind("<Button-1>", cpc1up)
root.bind("<Space>", lambda event, var1="space": counterup(var1))
root.mainloop()
